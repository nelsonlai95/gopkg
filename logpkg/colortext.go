package logpkg

const (
	RESET  = "\033[0m"
	BLACK  = "\033[30m"
	RED    = "\033[31m"
	GREEN  = "\033[32m"
	YELLOW = "\033[33m"
	BLUE   = "\033[34m"
	PURPLE = "\033[35m"
	CYAN   = "\033[36m"
	WHITE  = "\033[37m"
)

func Black(s string) string {
	return BLACK + s + RESET
}

func Red(s string) string {
	return RED + s + RESET
}

func Green(s string) string {
	return GREEN + s + RESET
}

func Yellow(s string) string {
	return YELLOW + s + RESET
}

func Blue(s string) string {
	return BLUE + s + RESET
}

func Purple(s string) string {
	return PURPLE + s + RESET
}

func Cyan(s string) string {
	return CYAN + s + RESET
}

func White(s string) string {
	return WHITE + s + RESET
}
