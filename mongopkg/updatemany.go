package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type updatemany struct {
	filter bson.M
	opts   *options.UpdateOptions
}

func NewUpdateMany() *updatemany {
	update := new(updatemany)
	update.opts = options.Update()
	return update
}

func (update *updatemany) SetFilter(filter bson.M) *updatemany {
	update.filter = filter
	return update
}

func (update *updatemany) SetUpsert(upsert bool) *updatemany {
	update.opts.SetUpsert(upsert)
	return update
}
