package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type insertmany struct {
	filter []interface{}
	opts   *options.InsertManyOptions
}

func NewInsertMany() *insertmany {
	insert := new(insertmany)
	insert.opts = options.InsertMany()
	return insert
}

func (insert *insertmany) SetFilter(filter bson.M) *insertmany {
	insert.filter = append(insert.filter, filter)
	return insert
}

func (insert *insertmany) SetOrdered(ordered bool) *insertmany {
	insert.opts.SetOrdered(ordered)
	return insert
}
