package mongopkg

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
)

type cursor struct {
	cur *mongo.Cursor
}

func NewCursor(cur *mongo.Cursor) *cursor {
	return &cursor{cur: cur}
}

func (c *cursor) Next() bool {
	if c.cur == nil {
		return false
	}
	return c.cur.Next(context.TODO())
}

func (c *cursor) Decode(result interface{}) error {
	if c.cur == nil {
		return nil
	}
	return c.cur.Decode(result)
}
