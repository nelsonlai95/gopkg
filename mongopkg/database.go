package mongopkg

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
)

type Database struct {
	conn   *MongoConnection
	client *mongo.Client
	db     *mongo.Database
}

func newDatabase(conn *MongoConnection, client *mongo.Client, databaseName string) *Database {
	return &Database{
		conn:   conn,
		client: client,
		db:     client.Database(databaseName),
	}
}

func (d *Database) Connect() {
	d.conn.Connect()
}

func (d *Database) ListCollectionNames() []string {
	names, err := d.db.ListCollectionNames(context.TODO(), nil)
	if err != nil {
		panic(err)
	}
	return names
}

func (d *Database) GetCollection(name string) *Collection {
	return newCollection(d.conn, d.client, d.db, name)
}

func (d *Database) CreateCollection(name string) (*Collection, error) {
	err := d.db.CreateCollection(context.TODO(), name)
	if err != nil {
		return nil, err
	}
	return newCollection(d.conn, d.client, d.db, name), nil
}

func (d *Database) Command(cmd interface{}) error {
	result := d.db.RunCommand(context.TODO(), cmd)
	return result.Err()
}

func (d *Database) Drop() error {
	return d.db.Drop(context.TODO())
}
