package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type deletemany struct {
	filter bson.M
	opts   *options.DeleteOptions
}

func NewDeleteMany() *deletemany {
	delete := new(deletemany)
	delete.opts = options.Delete()
	return delete
}

func (delete *deletemany) SetFilter(filter bson.M) *deletemany {
	delete.filter = filter
	return delete
}
