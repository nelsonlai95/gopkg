package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type aggreate struct {
	piplines []bson.M
	opts     *options.AggregateOptions
}

func NewAggregate() *aggreate {
	a := &aggreate{}
	a.opts = options.Aggregate()
	return a
}

func (a *aggreate) SetMatch(match bson.M) *aggreate {
	a.piplines = append(a.piplines, bson.M{"$match": match})
	return a
}

func (a *aggreate) SetProject(project bson.M) *aggreate {
	a.piplines = append(a.piplines, bson.M{"$project": project})
	return a
}

func (a *aggreate) SetGroup(group bson.M) *aggreate {
	a.piplines = append(a.piplines, bson.M{"$group": group})
	return a
}

func (a *aggreate) SetSort(sort bson.M) *aggreate {
	a.piplines = append(a.piplines, bson.M{"$sort": sort})
	return a
}

func (a *aggreate) SetLimit(limit int64) *aggreate {
	a.piplines = append(a.piplines, bson.M{"$limit": limit})
	return a
}

func (a *aggreate) SetSkip(skip int64) *aggreate {
	a.piplines = append(a.piplines, bson.M{"$skip": skip})
	return a
}

func (a *aggreate) SetUnwind(unwind string) *aggreate {
	a.piplines = append(a.piplines, bson.M{"$unwind": unwind})
	return a
}

func (a *aggreate) SetLookup(from string, localField string, foreignField string, as string) *aggreate {
	a.piplines = append(a.piplines, bson.M{"$lookup": bson.M{"from": from, "localField": localField, "foreignField": foreignField, "as": as}})
	return a
}
