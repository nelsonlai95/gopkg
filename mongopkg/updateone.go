package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type updateone struct {
	filter bson.M
	opts   *options.UpdateOptions
}

func NewUpdateOne() *updateone {
	udpate := &updateone{}
	udpate.opts = options.Update()
	return udpate
}

func (u *updateone) SetFilter(filter bson.M) *updateone {
	u.filter = filter
	return u
}

func (u *updateone) SetUpsert(upsert bool) *updateone {
	u.opts.SetUpsert(upsert)
	return u
}
