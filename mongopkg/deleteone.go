package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type deleteone struct {
	filter bson.M
	opts   *options.DeleteOptions
}

func NewDeleteOne() *deleteone {
	delete := new(deleteone)
	delete.opts = options.Delete()
	return delete
}

func (delete *deleteone) SetFilter(filter bson.M) *deleteone {
	delete.filter = filter
	return delete
}
