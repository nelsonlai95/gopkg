package stringbuilder

import "strings"

type goScriptBuilder struct {
	packageName string
	imports     map[string]string // map[importPath]assignedName
	lines       []string
}

func NewGoScriptBuilder(packageName string) *goScriptBuilder {
	return &goScriptBuilder{
		packageName: packageName,
		imports:     make(map[string]string),
		lines:       make([]string, 0),
	}
}

func (g *goScriptBuilder) AddImport(name string, assignedName string) *goScriptBuilder {
	g.imports[name] = assignedName
	return g
}

func (g *goScriptBuilder) AppendLine(line string) *goScriptBuilder {
	g.lines = append(g.lines, line)
	return g
}

func (g *goScriptBuilder) String() string {
	var builder strings.Builder
	builder.WriteString("package ")
	builder.WriteString(g.packageName)
	builder.WriteString("\n\n")
	for imp, assignedName := range g.imports {
		builder.WriteString("import ")
		if assignedName != "" {
			builder.WriteString(assignedName + " ")
		}
		builder.WriteString(`"` + imp + `"` + "\n")
	}
	builder.WriteString("\n")
	for _, line := range g.lines {
		builder.WriteString(line + "\n")
	}
	return builder.String()
}
