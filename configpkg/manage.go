package configpkg

var envs = make(map[string]struct{})

func register(env string) {
	envs[env] = struct{}{}
}

func ExportEnvs() []string {
	var output []string
	for env := range envs {
		output = append(output, env)
	}
	return output
}
