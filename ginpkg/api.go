package ginpkg

import "github.com/gin-gonic/gin"

func APIResponse(c *gin.Context, status int, code int, data interface{}) {
	c.JSON(status, gin.H{
		"code": code,
		"data": data,
	})
	c.Abort()
}
